<?php

declare(strict_types=1);

namespace App\Controllers\Professors;

use App\Controllers\IHTMLController;
use Psr\Http\Message\ResponseInterface;

final class GetProfessors extends IHTMLController
{
    public function __invoke(array $vars): ResponseInterface
    {
        // TODO
        return $this->HTMLResponse('REDIRECT TO OTHER APP');
    }
}