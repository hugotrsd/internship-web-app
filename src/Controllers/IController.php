<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Renderer\IRenderer;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;


abstract class IController
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var IRenderer
     */
    protected $renderer;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container, LoggerInterface $logger, ServerRequestInterface $request, IRenderer $renderer)
    {
        $this->container = $container;
        $this->logger = $logger;
        $this->request = $request;
        $this->renderer = $renderer;
    }

    abstract public function __invoke(array $vars): ResponseInterface;
}
