<?php

declare(strict_types=1);

namespace App\Controllers;

use GuzzleHttp\Psr7\Response;

use function GuzzleHttp\Psr7\stream_for;

abstract class IJSONController extends IController {
    protected function JSONResponse(object $response): Response
    {
        return (new Response())
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->withBody(
                stream_for(
                    json_encode(
                        $response,
                        \JSON_PRETTY_PRINT | \JSON_THROW_ON_ERROR
                    )
                )
            );
    }

    protected function JSONError(int $status, string $message = ''): Response
    {
        return $this->JSONResponse(
            (object) array(
                'error' => empty($message) ? 'Unknown error' : $message
            )
        )->withStatus($status);
    }
}