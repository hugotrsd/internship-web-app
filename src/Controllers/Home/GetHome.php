<?php

declare(strict_types=1);

namespace App\Controllers\Home;

use App\Controllers\IHTMLController;
use Psr\Http\Message\ResponseInterface;


final class GetHome extends IHTMLController
{
    public function __invoke(array $vars): ResponseInterface
    {
        return $this->HTMLResponse($this->renderer->render('portals' . DS . 'home' . DS . 'index'));
    }
}
