<?php

declare(strict_types=1);

namespace App\Controllers;

use GuzzleHttp\Psr7\Response;

use function GuzzleHttp\Psr7\stream_for;

abstract class IHTMLController extends IController {
    protected function HTMLResponse(string $body): Response
    {
        return (new Response())
            ->withStatus(200)
            ->withBody(stream_for($body));
    }

    protected function HTMLError(int $status, string $message = ''): Response
    {
        return $this->HTMLResponse($message)->withStatus($status);
    }
}