<?php

declare(strict_types=1);

namespace App\Controllers\Students;

use App\Controllers\IHTMLController;
use App\Controllers\Offers\ParsedownOfferDetailsExt;
use App\Database\Database;
use Psr\Http\Message\ResponseInterface;

final class GetStudentsOfferDetails extends IHTMLController
{
    public function __invoke(array $vars): ResponseInterface
    {
        $database = $this->container->get(Database::class);
        
        $offer = $database->getOffer($vars['offer_code'], true);

        // No data -> assume bad request
        if (empty($offer)) return $this->HTMLError(400, 'This offer does not exist');
        
        $vars['offer'] = $offer;
        $vars['offer']['description'] = ParsedownOfferDetailsExt::instance()->text($vars['offer']['description']);
        
        if ((new \DateTime()) > new \DateTime($offer['end_period']))
        $vars['offer_expired'] = true;
        
        if ($database->isOfferAssigned($vars['offer_code']))
        $vars['offer_assigned'] = true;
        
        return $this->HTMLResponse($this->renderer->render('portals' . DS . 'students' . DS . 'offer_details', $vars));
    }
}
