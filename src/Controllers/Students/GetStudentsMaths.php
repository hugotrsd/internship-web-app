<?php

declare(strict_types=1);

namespace App\Controllers\Students;

use App\Controllers\IHTMLController;
use Psr\Http\Message\ResponseInterface;

final class GetStudentsMaths extends IHTMLController
{
    public function __invoke(array $vars): ResponseInterface
    {
        $vars['markdown_content'] = \ParsedownExtra::instance()->text(file_get_contents('markdown' . DS . 'students' . DS . 'maths.md'));
        return $this->HTMLResponse($this->renderer->render('portals' . DS . 'students' . DS . 'maths', $vars));
    }
}