<?php

declare(strict_types=1);

namespace App\Controllers\Offers;

class ParsedownOfferDetailsExt extends \Parsedown {

    public function __construct() {
        $this->setMarkupEscaped(true);
        $this->setUrlsLinked(true);
        $this->setSafeMode(true);
    }

    protected function blockHeader($line) {
        $parsed = parent::blockHeader($line);
        switch($parsed['element']['name']) {           
            case 'h1':
                $parsed['element']['name'] = 'h5';
                break;
            
            default:
                $parsed['element']['name'] = 'h6';
                break;
        }
        return $parsed;
    }

    protected function blockSetextHeader($line, $block = null) {
        $block = parent::blockSetextHeader($line, $block);
        return $block;
    }
}