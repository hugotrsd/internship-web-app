<?php

declare(strict_types=1);

namespace App\Controllers\Offers;

use App\Controllers\IJSONController;
use Psr\Http\Message\ResponseInterface;

final class PostOfferSubmit extends IJSONController
{
    public function __invoke(array $vars): ResponseInterface
    {
        return $this->JSONResponse((object) []);
    }
}
