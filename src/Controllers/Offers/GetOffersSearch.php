<?php

declare(strict_types=1);

namespace App\Controllers\Offers;

use App\Controllers\IJSONController;
use App\Database\Database;
use App\Database\DatabaseException;
use Psr\Http\Message\ResponseInterface;

final class GetOffersSearch extends IJSONController
{
    public function __invoke(array $vars): ResponseInterface
    {
        $query = $this->request->getQueryParams();

        if (empty($query)) return $this->JSONResponse((object) []);

        try {
            $database = $this->container->get(Database::class);
            $result = $database->searchOffers($query); // Retrieve codes

            $cards = [];
            foreach ($result as $offer) {
                $card = $database->getOffer($offer, true);
                $card['is_new'] = (new \DateTime())->sub(new \DateInterval('P2D')) < new \DateTime($card['created_at']); // Mark as new if < 2 days
                $card['is_assigned'] = $database->isOfferAssigned($offer);
                $card['is_old'] = new \DateTime() > new \DateTime($card['end_period']); // Mark as old if today > end_period
                $cards[] = $card;
            }

            return $this->JSONResponse((object) array('offers' => $cards));
        } catch (DatabaseException $e) {
            $this->logger->critical($e, [$this->request]);
            return $this->JSONError(500, $e->getMessage());
        }
    }
}
