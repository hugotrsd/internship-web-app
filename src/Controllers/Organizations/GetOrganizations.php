<?php

declare(strict_types=1);

namespace App\Controllers\Organizations;

use App\Controllers\IHTMLController;
use Psr\Http\Message\ResponseInterface;

final class GetOrganizations extends IHTMLController
{
    public function __invoke(array $vars): ResponseInterface
    {
        // TODO
        return $this->HTMLResponse($this->renderer->render('portals' . DS . 'organizations' . DS . 'index'));
    }
}
