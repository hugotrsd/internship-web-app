<?php

declare(strict_types=1);

namespace App;

use GuzzleHttp\Psr7\ServerRequest;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use App\Middlewares\AddMissingRootSlashMiddleware;
use App\Middlewares\FastRouteMiddleware;
use App\Middlewares\RemoveTrailingSlashMiddleware;
use Exception;
use Middlewares\Whoops;

class App implements RequestHandlerInterface
{
    /**
     * Should be either 'dev' or 'prod'.
     * @var string
     */
    private $env;

    /**
     * The main config file
     * @var string
     */
    private $config;

    /** 
     * The main container, from php-di. This is initialized in the 'run' function
     * @see Config files
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string[]
     */
    private $middlewareList = [];

    /**
     * @var int
     */
    private $middlewareIndex = 0;

    public function __construct(string $env, string $configFile)
    {
        assert(in_array($env, ['dev', 'prod']));

        $this->env = $env;
        $this->config = $configFile;

        // Setup processing pipeline
        if ($this->env === 'dev')
            $this->pipe(Whoops::class);
        $this->pipe(AddMissingRootSlashMiddleware::class)
            ->pipe(RemoveTrailingSlashMiddleware::class)
            ->pipe(FastRouteMiddleware::class);
    }

    public function getEnv()
    {
        return $this->env;
    }

    /**
     * Register a middleware in the processing pipeline.
     * Middlewares are executed in the order they're added.
     */
    private function pipe(string $middleware): self
    {
        $this->middlewareList[] = $middleware;
        return $this;
    }

    /**
     * App entry point
     */
    public function process(ServerRequest $request): void
    {
        // Init container builder
        $containerBuilder = new \DI\ContainerBuilder();
        $containerBuilder->useAutowiring(true);
        $containerBuilder->addDefinitions($this->config); // Import config from file
        $containerBuilder->addDefinitions([ServerRequestInterface::class => $request]); // Some modules needs the request

        switch ($this->env) {
            case 'prod':
                $containerBuilder->enableCompilation(RUN_DIR . DS . 'container');
                $containerBuilder->writeProxiesToFile(true, RUN_DIR . 'container' . DS . 'proxies');
                break;

            default:
                $containerBuilder->writeProxiesToFile(false);
                break;
        }

        // Init container
        $this->container = $containerBuilder->build();

        try {
            self::sendBack($this->handle($request));
        } catch (\Exception $e) {
            $this->container->get(\Psr\Log\LoggerInterface::class)->alert("Uncaught exception: $e\n", [$request->getUri()]);
            throw $e;
        }
    }

    /**
     * Called recursively by each middleware that could not fully process the request.
     * @throws Exception When the request could not be fully processed by any middleware.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $middleware = $this->getNextMiddleware();

        if ($middleware === null)
            throw new \Exception("This request could not be handled by any middleware");

        return $middleware->process($request, $this);
    }

    /**
     * @return ?MiddlewareInterface The next middleware in the pipeline, if any.
     */
    private function getNextMiddleware(): ?MiddlewareInterface
    {
        if (!isset($this->middlewareList[$this->middlewareIndex])) return null;

        $middleware = $this->container->get($this->middlewareList[$this->middlewareIndex]);
        ++$this->middlewareIndex;

        return $middleware;
    }

    /**
     * Convert a ResponseInterface into HTTP, and send it to the client
     */
    private static function sendBack(ResponseInterface $response)
    {
        /* Stolen with <3 from: https://github.com/http-interop/response-sender/blob/master/src/functions.php */

        $http_line = sprintf(
            'HTTP/%s %s %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        );

        header($http_line, true, $response->getStatusCode());

        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header("$name: $value", false);
            }
        }

        $stream = $response->getBody();

        if ($stream->isSeekable()) {
            $stream->rewind();
        }

        while (!$stream->eof()) {
            echo $stream->read(1024 * 8);
        }
    }
}
