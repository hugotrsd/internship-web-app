<?php

declare(strict_types=1);

namespace App;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class LoggerFactory
{
    public function __invoke(): LoggerInterface
    {
        $lineFormatter = new LineFormatter(
            LineFormatter::SIMPLE_FORMAT,
            LineFormatter::SIMPLE_DATE,
            true,
            true
        );
        $lineFormatter->includeStacktraces(true);

        $stream = new StreamHandler(RUN_DIR . DS . 'logs' . DS . 'monolog.log');
        $stream->setFormatter($lineFormatter);

        $loggerHandlers = [$stream];

        $logger = new Logger('main', $loggerHandlers);

        return $logger;
    }
}
