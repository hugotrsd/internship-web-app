<?php

declare(strict_types=1);

namespace App\Middlewares;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

/**
 * The standard specifies that requests begining with '/' and those without 
 * should be treated as equal. However, UriInterface::getPath can return
 * the two variants. So this middleware puts a '/' in front.
 */
class AddMissingRootSlashMiddleware implements MiddlewareInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $uri = $request->getUri()->getPath();
        if (empty($uri) || $uri[0] !== '/') {
            $this->logger->notice("Missing root slash for uri '$uri' -> redirecting");
            
            return (new Response())
                ->withStatus(301) // Moved Permanently
                ->withHeader('Location', '/' . $uri . '?' . $request->getUri()->getQuery());
        }
        
        return $handler->handle($request);
    }
}
