<?php

declare(strict_types=1);

namespace App\Middlewares;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

/**
 * Remove trailing slash in the request, because it will confuse the router
 */
class RemoveTrailingSlashMiddleware implements MiddlewareInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $path = $request->getUri()->getPath();
        $query = $request->getUri()->getQuery();
        if (isset($path[1]) && $path[-1] === '/') {
            $this->logger->notice("Trailing slash detected for uri '$path' -> redirecting");
            
            $trimed = rtrim($path, "/");
            if (empty($trimed)) $trimed = '/';
            if (!empty($query)) $query = '?' . $query;
            
            return (new Response())
                ->withStatus(301) // Moved Permanently
                ->withHeader('Location', $trimed . $query);
        }

        return $handler->handle($request);
    }
}
