<?php

declare(strict_types=1);

namespace App\Middlewares;

use App\Controllers\Home\GetHome;
use App\Controllers\Offers\GetOffersSearch;
use App\Controllers\Offers\PostOfferSubmit;
use App\Controllers\Students\GetStudents;
use App\Controllers\Students\GetStudentsIT;
use App\Controllers\Students\GetStudentsMaths;
use App\Controllers\Students\GetStudentsPCEEA;
use App\Controllers\Students\GetStudentsOffers;
use App\Controllers\Students\GetStudentsOfferDetails;
use App\Controllers\Professors\GetProfessors;
use App\Controllers\Organizations\GetOrganizations;
use Exception;
use FastRoute\RouteCollector;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Main router
 */
class FastRouteMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var \FastRoute\Dispatcher
     */
    private $router;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $rc) {
            $rc->addGroup('/', function (RouteCollector $r) {
                $r->addRoute('GET', '', GetHome::class);
            });
            $rc->addGroup('/offers', function (RouteCollector $r) {
                $r->addRoute('GET', '/search', GetOffersSearch::class);
                $r->addRoute('POST', '/submit', PostOfferSubmit::class);
            });
            $rc->addGroup('/students', function (RouteCollector $r) {
                $r->addRoute('GET', '', GetStudents::class);
                $r->addRoute('GET', '/it', GetStudentsIT::class);
                $r->addRoute('GET', '/maths', GetStudentsMaths::class);
                $r->addRoute('GET', '/pceea', GetStudentsPCEEA::class);
                $r->addRoute('GET', '/offers', GetStudentsOffers::class);
                $r->addRoute('GET', '/offer-{offer_code:\d+}', GetStudentsOfferDetails::class);
            });
            $rc->addGroup('/professors', function (RouteCollector $r) {
                $r->addRoute('GET', '', GetProfessors::class);
            });
            $rc->addGroup('/organizations', function (RouteCollector $r) {
                $r->addRoute('GET', '', GetOrganizations::class);
            });
        });
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeInfo = $this->router->dispatch($request->getMethod(), $request->getUri()->getPath());

        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND:
                return (new Response())->withStatus(404);

            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                return (new Response())->withStatus(405);

            default:
                $controller = $this->container->get($routeInfo[1]);
                return $controller($routeInfo[2]);
        }
    }
}
