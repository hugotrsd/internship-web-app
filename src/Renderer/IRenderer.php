<?php

declare(strict_types=1);

namespace App\Renderer;

/**
 * Renderer interface for rendering engine abstraction.
 */
interface IRenderer
{
    /**
     * Renders a view,
     * Paths can be provided with a namespace added with the addPath function.
     */
    public function render(string $view, array $params = []): string;

    /**
     * Add a global variable to all views.
     */
    public function addGlobal(string $key, $value): void;
}
