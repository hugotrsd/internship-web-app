<?php

declare(strict_types=1);

namespace App\Renderer;

use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigRendererFactory
{
    public function __invoke(ContainerInterface $container): TwigRenderer
    {
        $viewsPath = $container->get('path.views');
        $loader = new FilesystemLoader($viewsPath);
        $twig = new Environment(
            $loader,
            [ /* TODO */]
        );
        $twig->addExtension(new \Twig\Extension\DebugExtension());

        return new TwigRenderer($twig);
    }
}
