<?php

declare(strict_types=1);

namespace App\Renderer;

use Twig\Environment;

/**
 * Twig implementation of the rendering interface
 */
class TwigRenderer implements IRenderer
{
    /**
     * Twig config
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getEnv(): Environment
    {
        return $this->twig;
    }

    public function render(string $view, array $params = []): string
    {
        return $this->twig->render($view . '.twig', $params);
    }

    public function addGlobal(string $key, $value): void
    {
        $this->twig->addGlobal($key, $value);
    }
}
