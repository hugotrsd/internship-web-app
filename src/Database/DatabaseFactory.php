<?php

declare(strict_types=1);

namespace App\Database;

use App\Database\Database;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class DatabaseFactory
{
    public function __invoke(ContainerInterface $container, LoggerInterface $logger): Database
    {
        $name = $container->get('db.name');
        $host = $container->get('db.host');
        $user = $container->get('db.user');
        $pass = $container->get('db.pass');

        return new Database($logger, $name, $host, $user, $pass);
    }
}
