<?php

declare(strict_types=1);

namespace App\Database;

class DatabaseException extends \RuntimeException
{
}
