<?php

declare(strict_types=1);

namespace App\Database;

use Psr\Log\LoggerInterface;

/**
 * Main interface to work with the database.
 */
class Database
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool 
     */
    private $isInTransaction;

    public function __construct(LoggerInterface $logger, string $name, string $host, ?string $username, ?string $password)
    {
        $this->logger = $logger;
        try {
            $this->pdo = new \PDO("mysql:dbname=$name;host=$host", $username, $password);

            $status = true;
            $status = $status && $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $status = $status && $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            if (!$status)
                throw new DatabaseException('Failed to set one or more PDO attributes');
        } catch (\Exception $e) {
            $this->logger->alert($e);
            throw new DatabaseException('Failed to initialize database connection', 0, $e);
        }

        $this->isInTransaction = false;
    }

    /**
     * Executes a function within an SQL transaction
     * @param object $obj The object on which to call the function
     * @param array $callback The function to wrap
     * @return mixed Whatever $callback returns
     */
    private function wrapInsideTransaction(object $obj, array $callback)
    {
        assert(!$this->isInTransaction, 'Already in a transaction -> this should never happen');

        try {
            $this->isInTransaction = true;

            if (!$this->pdo->beginTransaction())
                throw new DatabaseException('Failed to begin transaction');

            $tr = call_user_func($callback, $obj);
            if ($tr === false)
                throw new DatabaseException('Failed to execute callback function');

            if (!$this->pdo->commit())
                throw new DatabaseException('Failed to commit transaction');

            return $tr;
        } catch (\PDOException $e) {
            $this->pdo->rollBack();
            $this->logger->error($e);
            throw new DatabaseException('Transaction failed', 0, $e);
        } finally {
            $this->isInTransaction = false;
        }
    }

    /**
     * Executes an SQL statement
     * @param string $sql The SQL query to execute
     * @param ?array $params Optional parameters for prepared statement
     * @return \PDOStatement The generated (executed) statement
     */
    private function execStatement(string $sql, ?array $params = null): \PDOStatement
    {
        if ($params === null) {
            $stmt = $this->pdo->query($sql);
            if ($stmt === false)
                throw new DatabaseException('Failed to execute statement');
        } else {
            $stmt = $this->pdo->prepare($sql);
            if ($stmt === false)
                throw new DatabaseException('Failed to create prepared statement');

            if ($stmt->execute($params) === false)
                throw new DatabaseException("Failed to execute prepared statement");
        }
        return $stmt;
    }

    // -------------------- Insert -------------------- //

    /**
     * Inserts $entity into $table, recursively resolving $foreignKeys
     * @param string $table The table name 
     * @param array $entity key => value array describing how fields should be filled.
     * If key appears in $foreignKeys, value is either another key => value array, or the
     * related entity's primary key
     * @param array $foreignKeys key => value array where key is a foreign key field to resolve
     * in the newly created entity, and value is the function to call to create that foreign entity
     * @return string The assigned AUTO_INCREMENT id for the new entity. If AUTO_INCREMENT was not used
     * because the id was set manually, returns 0
     */
    private function insert(string $table, array $entity, array $foreignKeys): string
    {
        // Generate an SQL statement using $entity attributes
        $keys = array_keys($entity);
        $columns = implode(', ', $keys); // Create (attr1, attr2, ...)
        $values = implode(', ', array_fill(0, sizeof($keys), '?')); // Create (?, ?, ...)

        // Resolve foreign keys
        foreach ($foreignKeys as $key => $callback) {
            if (is_object($entity[$key])) { // is_object(null) -> false, which is what we want
                $res = call_user_func($callback, $entity[$key]); // Recursively resolve the foreign key
                if ($res === false)
                    throw new DatabaseException('Failed to execute foreign key resolution function');
                $entity[$key] = $res;
            }
        }

        $sql = "INSERT INTO $this->dbname.$table($columns) VALUES ($values)";
        $this->execStatement($sql, array_values($entity));

        //!\\ -> this returns '0' if the primary key was set manually
        return $this->pdo->lastInsertId();
    }

    public function insertAddress(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = [];
            $asArray = get_object_vars($obj);
            return $this->insert('Address', $asArray, $foreign);
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertContact(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = [
                'address' => array($this, 'insertAddress')
            ];
            $asArray = get_object_vars($obj);
            return $this->insert('Contact', $asArray, $foreign);
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertExternal(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = [];
            $asArray = get_object_vars($obj);

            // Resolve the primary key before, to be able to return it
            if (is_object($asArray['id']))
                $asArray['id'] = $this->insertIPerson($asArray['id']);

            $this->insert('External', $asArray, $foreign);
            return $asArray['id'];
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertOrganization(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = [
                'contact' => array($this, 'insertContact'),
                'referee' => array($this, 'insertExternal')
            ];
            $asArray = get_object_vars($obj);
            return $this->insert('Organization', $asArray, $foreign);
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertIPerson(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = ['contact' => array($this, 'insertContact')];
            $asArray = get_object_vars($obj);
            return $this->insert('IPerson', $asArray, $foreign);
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertCredentials(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = [];
            $asArray = get_object_vars($obj);
            return $this->insert('Credentials', $asArray, $foreign);
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    public function insertSupervisor(object $obj)
    {
        if ($this->isInTransaction) {
            $foreign = ['credentials' => array($this, 'insertCredentials')];
            $asArray = get_object_vars($obj);

            // Resolve the primary key before, to be able to return it
            if (is_object($asArray['id']))
                $asArray['id'] = $this->insertIPerson($asArray['id']);

            $this->insert('Supervisor', $asArray, $foreign);
            return $asArray['id'];
        } else
            return $this->wrapInsideTransaction($obj, array($this, __METHOD__));
    }

    // -------------------- Update -------------------- //


    // -------------------- Query -------------------- //

    /**
     * Gets an entity from the database, recursively resolving the relevant foreign keys
     * @param string $table The table name
     * @param string $primaryKeyName Used in WHERE clause WHERE $primaryKeyName = $primaryKeyValue
     * @param array $foreignKeys key => value array where key is a foreign key field to resolve
     * in the entity, and value is the function to call to get that foreign entity
     * @return array key => value array mapping for the entity data
     */
    private function get(string $table, string $primaryKeyName, string $primaryKeyValue, array $foreignKeys)
    {
        $sql = "SELECT * FROM $table WHERE $primaryKeyName = ?";
        $result = $this->execStatement($sql, [$primaryKeyValue])->fetch();
        if ($result === false) // No entry
            return [];

        // Resolve foreign keys
        foreach ($foreignKeys as $key => $callback) {
            if (!is_null($result[$key])) {
                $res = call_user_func($callback, $result[$key], true); // Recursively resolve the foreign key
                if ($res === false)
                    throw new DatabaseException('Failed to execute foreign key resolution function');
                $result[$key] = $res;
            }
        }

        return $result;
    }

    public function getOffer(string $offerCode, bool $recursive = false)
    {
        $foreign = [
            'organization' => array($this, 'getOrganization'),
            'tutor' => array($this, 'getExternal')
        ];

        return $this->get('InternshipOffer', 'code', $offerCode, $recursive ? $foreign : []);
    }

    public function getOrganization(string $organizationID, bool $recursive = false)
    {
        $foreign = [
            'contact' => array($this, 'getContact'),
            'referee' => array($this, 'getExternal')
        ];

        return $this->get('Organization', 'id', $organizationID, $recursive ? $foreign : []);
    }

    public function getContact(string $contactID, bool $recursive = false)
    {
        $foreign = ['address' => array($this, 'getAddress')];

        return $this->get('Contact', 'id', $contactID, $recursive ? $foreign : []);
    }

    public function getExternal(string $externalID, bool $recursive = false)
    {
        $foreign = ['id' => array($this, 'getIPerson')];

        return $this->get('External', 'id', $externalID, $recursive ? $foreign : []);
    }

    public function getAddress(string $addressID, bool $recursive = false)
    {
        return $this->get('Address', 'id', $addressID, []);
    }

    public function getIPerson(string $personID, bool $recursive = false)
    {
        $foreign = ['contact' => array($this, 'getContact')];

        return $this->get('IPerson', 'id', $personID, $recursive ? $foreign : []);
    }

    // -------------------- Utils -------------------- //

    public function isOfferAssigned(string $offerCode)
    {

        $sql = "SELECT code FROM UnassignedInternshipOffers WHERE code = ?";
        $result = $this->execStatement($sql, [$offerCode])->fetch();
        return empty($result);
    }

    public function searchOffers(array $filters)
    {
        $table = isset($filters['include_assigned']) ? "InternshipOffer" : "UnassignedInternshipOffers";

        /* 
         * - Use the "condition OR TRUE|FALSE" trick to turn off relevant WHERE clauses, using lazy evaluation (CASE clause is another variant)
         * - related_sector checkboxes are OR-ed together to allow [X] IT AND [X] MATHS => IT OR MATHS
         * - Only care about end_period for period checking to make the search more flexible
         * - The MATCH clause is at the end since it's a time consuming check, we want it to occur on a limited set of results
         */
        $sql = "SELECT code FROM $table WHERE
            (
                    ((b'100' & related_sector) != 0 AND ?)
                OR  ((b'010' & related_sector) != 0 AND ?)
                OR  ((b'001' & related_sector) != 0 AND ?)
            )
            AND (paid = 1 OR NOT ?) 
            AND (end_period >= CURDATE() OR ?)
            AND (start_period <= ? OR ?)
            AND (? <= end_period OR ?)
            AND (
                CASE WHEN ?
                THEN MATCH (code, subject, description) AGAINST (? ? IN BOOLEAN MODE)
                ELSE TRUE
                END
            )
        ";

        // Order must correspond to the one in the SQL query
        $params = [];
        $params[] = isset($filters['sectors_it']);
        $params[] = isset($filters['sectors_maths']);
        $params[] = isset($filters['sectors_pceea']);
        $params[] = isset($filters['paid']);
        $params[] = isset($filters['include_expired']);

        if (isset($filters['period_start_period']) && !empty($filters['period_start_period'])) {
            $params[] = $filters['period_start_period'];
            $params[] = false;
        } else {
            $params[] = '';
            $params[] = true;
        }

        if (isset($filters['period_end_period']) && !empty($filters['period_end_period'])) {
            $params[] = $filters['period_end_period'];
            $params[] = false;
        } else {
            $params[] = '';
            $params[] = true;
        }

        if (isset($filters['query_string'])) {
            $keywords = explode(' ', $filters['query_string']);

            $wildcards = [];
            $exactMatches = [];

            foreach ($keywords as $word) {
                $word = preg_replace('/[^\p{L}\p{N}\s]/u', '', $word);  // Remove symbols, because MATCH doesn't like those
                if (!empty($word) && mb_strlen($word, 'UTF-8') >= 2) {
                    $wildcards[] = $word . '*';  // Generate "dev*" to match "Développement"
                    $exactMatches[] = $word; // Generate "dev" to match "Dév"
                }
            }

            if (!empty($wildcards)) {
                $params[] = true;
                $params[] = implode(' ', $wildcards);
                $params[] = implode(' ', $exactMatches);
            } else {
                $params[] = false;
                $params[] = '';
                $params[] = '';
            }
        }

        $result = $this->execStatement($sql, $params)->fetchAll();
        if ($result === false)
            throw new DatabaseException('Failed to fetchAll from result set');


        $tr = [];
        foreach ($result as $entry) {
            $code = $entry['code'];

            // Now exclude results based on distance from university
            if (isset($filters['distance']) && is_numeric($filters['distance']) && ((int) $filters['distance']) !== 0) {
                $sql = "SELECT lat, lon FROM Address WHERE id = (
                    SELECT address FROM Contact WHERE id = (
                        SELECT contact FROM Organization WHERE id = (
                            SELECT organization FROM InternshipOffer WHERE code = ?
                        )
                    )
                )";

                $coords = $this->execStatement($sql, [$code])->fetch();
                if ($coords !== false) {
                    $univCoords = $this->execStatement("SELECT lat, lon FROM Address WHERE id = '1'", [])->fetch(); // University address should be in the database at id 1
                    if ($univCoords === false)
                        throw new DatabaseException('Failed to retrieve university coordinates: missing data');

                    // See: https://stackoverflow.com/a/27943, https://en.wikipedia.org/wiki/Haversine_formula
                    // Computing distance using Haversine formula
                    $distanceBetween = function ($startLat, $startLon, $endLat, $endLon) {
                        $deg2rad = function ($deg) {
                            return $deg * (pi() / 180.);
                        };

                        $diffLat = $deg2rad($endLat - $startLat);
                        $diffLon = $deg2rad($endLon - $startLon);

                        $a = sin($diffLat / 2) * sin($diffLat / 2)
                            + cos(deg2rad($startLat)) * cos(deg2rad($startLat)) * sin($diffLon / 2) * sin($diffLon / 2);
                        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
                        $d = 6371 * $c; // 6371 -> earth radius
                        return $d;
                    };

                    $dist = $distanceBetween(
                        (float) $univCoords['lat'],
                        (float) $univCoords['lon'],
                        (float) $coords['lat'],
                        (float) $coords['lon']
                    );

                    if ($dist > (int) $filters['distance'])
                        continue;
                }
            }
            $tr[] = $code;
        }

        return $tr;
    }
}
