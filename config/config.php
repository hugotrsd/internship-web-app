<?php

declare(strict_types=1);

return [
    // Path settings
    'path.views'   => 'views',

    // Logging
    \Psr\Log\LoggerInterface::class => DI\factory(\App\LoggerFactory::class),

    // Rendering
    \App\Renderer\IRenderer::class => \DI\factory(\App\Renderer\TwigRendererFactory::class),
    
    // Database stuff
    'db.name' => \DI\env('DB_NAME', 'internships'),
    'db.host' => \DI\env('DB_HOST'),
    'db.user' => \DI\env('DB_USER'),
    'db.pass' => \DI\env('DB_PASS'),
    \App\Database\Database::class => \DI\factory(\App\Database\DatabaseFactory::class),
];
