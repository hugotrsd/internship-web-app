DROP DATABASE IF EXISTS internships;
CREATE DATABASE IF NOT EXISTS internships;
ALTER DATABASE internships CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- --------------------------------------------------------------------------------------------------- --
-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Address;

CREATE TABLE IF NOT EXISTS internships.Address (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  lat FLOAT NOT NULL,
  lon FLOAT NOT NULL,
  address_line_1 VARCHAR(120) NOT NULL,
  address_line_2 VARCHAR(120) NULL,
  country VARCHAR(80) NOT NULL,
  administrative VARCHAR(80) NULL,
  county VARCHAR(80) NULL,
  city VARCHAR(80) NOT NULL,
  postcode VARCHAR(16) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Contact;

CREATE TABLE IF NOT EXISTS internships.Contact (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  address INT UNSIGNED NULL,
  email_address VARCHAR(255) NULL,
  phone_number_1 VARCHAR(15) NULL,
  phone_number_2 VARCHAR(15) NULL,
  fax_number CHAR(10) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_Contact_Address
    FOREIGN KEY (address)
    REFERENCES internships.Address (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Contact_Address_idx ON internships.Contact (address);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.IPerson;

CREATE TABLE IF NOT EXISTS internships.IPerson (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  last_name VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  contact INT UNSIGNED NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_IPerson_Contact
    FOREIGN KEY (contact)
    REFERENCES internships.Contact (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_IPerson_Contact_idx ON internships.IPerson (contact);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.External;

CREATE TABLE IF NOT EXISTS internships.External (
  id INT UNSIGNED NOT NULL,
  profession VARCHAR(255) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_External_IPerson
    FOREIGN KEY (id)
    REFERENCES internships.IPerson (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_External_IPerson_idx ON internships.External (id);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Organization;

CREATE TABLE IF NOT EXISTS internships.Organization (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  service VARCHAR(255) NULL,
  contact INT UNSIGNED NOT NULL,
  referee INT UNSIGNED NULL COMMENT 'The person managing internships for the organization, if any',
  PRIMARY KEY (id),
  CONSTRAINT fk_Organization_Contact
    FOREIGN KEY (contact)
    REFERENCES internships.Contact (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_Organization_External
    FOREIGN KEY (referee)
    REFERENCES internships.External (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Organization_Contact_idx ON internships.Organization (contact);

CREATE INDEX fk_Organization_External_idx ON internships.Organization (referee);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.InternshipOffer;

CREATE TABLE IF NOT EXISTS internships.InternshipOffer (
  code CHAR(8) NOT NULL,
  subject VARCHAR(100) NOT NULL,
  description TEXT NOT NULL,
  related_sector BIT(3) NOT NULL DEFAULT b'111' COMMENT '100=IT | 010=MATHS | 001=PCEEA. If set, students of that sector might be interested by this offer',
  start_period DATE NOT NULL COMMENT 'Date on which an intern CAN start',
  end_period DATE NOT NULL COMMENT 'Date on which the intern MUST leave',
  paid BOOLEAN NOT NULL DEFAULT 0,
  organization INT UNSIGNED NOT NULL,
  tutor INT UNSIGNED NOT NULL COMMENT 'The person who will be charge of the intern',
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (code),
  CONSTRAINT fk_InternshipOffer_Organization
    FOREIGN KEY (organization)
    REFERENCES internships.Organization (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_InternshipOffer_External
    FOREIGN KEY (tutor)
    REFERENCES internships.External (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_InternshipOffer_Organization_idx ON internships.InternshipOffer (organization);

CREATE INDEX fk_InternshipOffer_External_idx ON internships.InternshipOffer (tutor);

CREATE FULLTEXT INDEX ft_InternshipOffer ON internships.InternshipOffer (code, subject, description);
-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.InternshipAgreement;

CREATE TABLE IF NOT EXISTS internships.InternshipAgreement (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  signed_by BIT(3) NOT NULL DEFAULT 0 COMMENT '100=by_supervisor | 010=by_administration | 001=by_student',
  delivered_on DATE NULL,
  delivered_by ENUM('HAND', 'MAIL', 'EMAIL') NULL,
  PRIMARY KEY (id),
  CONSTRAINT InternshipAgreement_delivered_both_fields_set
    CHECK ((delivered_on IS NULL AND delivered_by IS NULL) OR (delivered_on IS NOT NULL AND delivered_by IS NOT NULL)))
ENGINE = InnoDB;

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Credentials;

CREATE TABLE IF NOT EXISTS internships.Credentials (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  login CHAR(8) NOT NULL,
  password BINARY(60) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Supervisor;

CREATE TABLE IF NOT EXISTS internships.Supervisor (
  id INT UNSIGNED NOT NULL,
  credentials INT UNSIGNED NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_Supervisor_Person
    FOREIGN KEY (id)
    REFERENCES internships.IPerson (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_Supervisor_Credentials
    FOREIGN KEY (credentials)
    REFERENCES internships.Credentials (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Supervisor_IPerson_idx ON internships.Supervisor (id);

CREATE INDEX fk_Supervisor_Credentials_idx ON internships.Supervisor (credentials);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Student;

CREATE TABLE IF NOT EXISTS internships.Student (
  id INT UNSIGNED NOT NULL,
  sector BIT(3) NOT NULL COMMENT '100=IT | 010=MATHS | 001=PCEEA',
  id_JFC CHAR(13),
  id_INE CHAR(11),
  PRIMARY KEY (id),
  CONSTRAINT fk_Student_IPerson
    FOREIGN KEY (id)
    REFERENCES internships.IPerson (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Student_IPerson_idx ON internships.Student (id);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Internship;

CREATE TABLE IF NOT EXISTS internships.Internship (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  offer CHAR(8) NOT NULL,
  student INT UNSIGNED NOT NULL,
  supervisor INT UNSIGNED NOT NULL,
  agreement INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_Internship_InternshipOffer
    FOREIGN KEY (offer)
    REFERENCES internships.InternshipOffer (code)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_Internship_InternshipAgreement
    FOREIGN KEY (agreement)
    REFERENCES internships.InternshipAgreement (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_Internship_Supervisor
    FOREIGN KEY (supervisor)
    REFERENCES internships.Supervisor (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_Internship_Student
    FOREIGN KEY (student)
    REFERENCES internships.Student (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Internship_InternshipOffer_idx ON internships.Internship (offer);

CREATE INDEX fk_Internship_InternshipAgreement_idx ON internships.Internship (agreement);

CREATE INDEX fk_Internship_Supervisor_idx ON internships.Internship (supervisor);

CREATE INDEX fk_Internship_Student_idx ON internships.Internship (student);

-- --------------------------------------------------------------------------------------------------- --

DROP TABLE IF EXISTS internships.Admin;

CREATE TABLE IF NOT EXISTS internships.Admin (
  id INT UNSIGNED NOT NULL,
  credentials INT UNSIGNED NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_Admin_IPerson
    FOREIGN KEY (id)
    REFERENCES internships.IPerson (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_Admin_Credentials
    FOREIGN KEY (credentials)
    REFERENCES internships.Credentials (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX fk_Admin_IPerson_idx ON internships.Admin (id);

CREATE INDEX fk_Admin_Credentials_idx ON internships.Admin (credentials);

-- --------------------------------------------------------------------------------------------------- --
-- --------------------------------------------------------------------------------------------------- --

DROP VIEW IF EXISTS internships.UnassignedInternshipOffers;

CREATE OR REPLACE VIEW internships.UnassignedInternshipOffers AS
  SELECT 
    *
  FROM
    internships.InternshipOffer O
  LEFT JOIN
    internships.Internship I
  ON O.code = I.offer
  WHERE
    I.offer IS NULL
;

-- --------------------------------------------------------------------------------------------------- --
-- --------------------------------------------------------------------------------------------------- --

DROP PROCEDURE IF EXISTS internships.clearUnassignedIntershipOffers;

CREATE PROCEDURE internships.clearUnassignedIntershipOffers ()
  DELETE FROM
    internships.InternshipOffer
  WHERE code IN (
      SELECT
        O.code
      FROM
        internships.InternshipOffer O
      LEFT JOIN
        internships.Internship I
      ON O.code = I.offer
      WHERE
        (I.offer IS NULL) AND (O.end_period < CURDATE())
  )
;

/*

-- --------------------------------------------------------------------------------------------------- --
-- --------------------------------------------------------------------------------------------------- --

DROP USER IF EXISTS 'php_guest'@'localhost';

CREATE USER 'php_guest'@'localhost' IDENTIFIED BY 'pass_php_guest';

GRANT SELECT ON TABLE internships.* TO 'php_guest'@'localhost';

-- --------------------------------------------------------------------------------------------------- --

DROP USER IF EXISTS 'php_edit'@'localhost';

CREATE USER 'php_edit'@'localhost' IDENTIFIED BY 'pass_php_edit';

GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE internships.* TO 'php_edit'@'localhost';

-- --------------------------------------------------------------------------------------------------- --
-- --------------------------------------------------------------------------------------------------- --

*/