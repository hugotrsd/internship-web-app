-- Add university address --

INSERT INTO internships.Address VALUES (
  1,
  43.9197,
  2.13954,
  'Place Verdun',
  'INU Champollion',
  'France',
  'Occitanie',
  'Tarn',
  'Albi',
  '81000'
);

-- --------------------------------------------------------------------------------------------------- --

INSERT INTO internships.Contact VALUES (
  1,
  NULL,
  'david.panzoli@univ-jfc.fr',
  NULL,
  NULL,
  NULL
);

INSERT INTO internships.Contact VALUES (
  2,
  NULL,
  'nicolas.garric@univ-jfc.fr',
  NULL,
  NULL,
  NULL
);

INSERT INTO internships.Contact VALUES (
  3,
  NULL,
  'laura.brillon@univ-jfc.fr',
  NULL,
  NULL,
  NULL
);

INSERT INTO internships.Contact VALUES (
  4,
  1,
  'julien.cegarra@univ-jfc.fr',
  '+33563481971',
  NULL,
  NULL
);

INSERT INTO internships.Contact VALUES (
  5,
  NULL,
  'jcegarra@gmail.com',
  NULL,
  NULL,
  NULL
);

INSERT INTO internships.Contact VALUES (
  6,
  NULL,
  'hugo.trassoudaine@etud.univ-jfc.fr',
  NULL,
  NULL,
  NULL
);

INSERT INTO internships.IPerson VALUES (
  1,
  'PANZOLI',
  'David',
  1
);

INSERT INTO internships.IPerson VALUES (
  2,
  'GARRIC',
  'Nicolas',
  2
);

INSERT INTO internships.IPerson VALUES (
  3,
  'BRILLON',
  'Laura',
  3
);

INSERT INTO internships.IPerson VALUES (
  4,
  'CEGARRA',
  'Julien',
  5
);

INSERT INTO internships.IPerson VALUES (
  5,
  'TRASSOUDAINE',
  'Hugo',
  6
);

INSERT INTO internships.Supervisor VALUES (
  2,
  NULL
);

INSERT INTO internships.Supervisor VALUES (
  3,
  NULL
);

INSERT INTO internships.External VALUES (
  1,
  'Professeur des universités'
);

INSERT INTO internships.External VALUES (
  2,
  'Professeur des universités'
);

INSERT INTO internships.External VALUES (
  4,
  'Professeur des universités'
);

INSERT INTO internships.Student VALUES (
  5,
  b'100',
  NULL,
  NULL
);

INSERT INTO internships.Organization VALUES (
  1,
  'Laboratoire SCoTE, Sciences de la Cognition, Technologie, Ergonomie',
  NULL,
  4,
  4
);

INSERT INTO internships.Organization VALUES (
  2,
  'Université Jean-François Champollion',
  'Département informatique',
  1,
  1
);

INSERT INTO internships.InternshipOffer VALUES (
  '20200001',
  'Développement C# / Unity3D pour le projet PER4MANCE',
  '###Stage\r\nLe laboratoire Sciences de la cognition, Technologie, Ergonomie (EA 7420) vise à établir un pôle de recherche en Sciences de la cognition et Ergonomie. Il vise la production de connaissances fondamentales sur le fonctionnement cérébral et les capacités d\'adaptation humaines - le contrôle cognitif - selon les changements de buts et de contextes. Le laboratoire s’inscrit également dans une recherche finalisée au service de la société. Les travaux de l’unité permettent d’intervenir avec des partenaires industriels en proposant des recherches qui peuvent s’inscrire dans des Niveaux de\r\nMaturité Technologique les plus bas jusqu’à des niveaux très avancés de prototypage (en environnement opérationnel)\r\nCe stage est proposé pour contribuer au Projet PER4MANCE [ https://www.isaesupaero.fr/fr/actualites/per4mance-projet-de-recherche-porte-par-l-isae-supaero-finance-par-l-anr/ ]. Ce projet est mené par l’INUC, ISAE, l’IRIT et le LASS en partenariat avec deux industriels (Airbus, Dassault). L’objectif est de produire un ensemble de modèles et d’outils pour améliorer la performance des lignes de fabrication aéronautique. Pour l’INUC, il s’agit notamment d’améliorer la gestion des risques aux postes de travail. Cela impose de disposer d’outils pour évaluer les risques pour les opérateurs (par exemple : temps passé avec certaines postures difficiles). Dans le cadre de ce stage, il s’agira de participer à l’élaboration d’un outil visant à remplacer l’évaluation manuelle par une évaluation semi-automatisée.\r\n\r\n###Missions\r\nAu sein du laboratoire SCoTE, les missions seront notamment les suivants :\r\n* Développer une interface utilisateur selon un ensemble de spécifications ;\r\n* Aider à l\'amélioration de l’expérience utilisateur ;\r\n* Intégrer des librairies pre-existantes d’analyse d’image ;\r\n* Participer à la revue de code et à la gestion de versions collaborative ;\r\n* Monter en compétences sur l’écosystème Unity3D / C#\r\n\r\n###Profil recherché\r\n* Connaissance du langage C#. Connaissances de Unity3D (ou d’environnement similaire comme Godot/Unreal Engine) et du C++ serait un plus.\r\n* Bon relationnel, force de proposition et capacité à travailler en équipe.\r\n* Savoir évoluer dans un environnement dynamique.\r\n* Faire preuve d’autonomie, de rigueur et de créativité.\r\n\r\n###Durée\r\n- Entre 2 et 4 mois. Horaires aménageables pendant période de cours. Démarrage dès que possible.\r\n- Gratification de stage en vigueur',
  b'100',
  '2020-01-18',
  '2020-08-31',
  1,
  1,
  4,
  DEFAULT,
  DEFAULT
);

INSERT INTO internships.InternshipOffer VALUES (
  '20200002',
  'Développement d''une application de gestion des stages',
  'Surement plein de choses à remplir dans ce champ, mais vraiment, tout ça n''est qu''un jeu de test rien de plus. Vraiment, il faudrait beaucoup s''attarder sur ces données',
  b'111',
  '2019-09-01',
  '2020-12-31',
  0,
  2,
  2,
  DEFAULT,
  DEFAULT
);

INSERT INTO internships.InternshipAgreement VALUES (
  1,
  b'000',
  NULL,
  NULL
);

INSERT INTO internships.Internship VALUES (
  1,
  '2020-01-15',
  '2020-04-14',
  '20200002',
  5,
  2,
  1
);




/*

-- Creation of an organization --

INSERT INTO Address(address_line_1, country, city, postcode) VALUES (
  '15 Place Fernand Pelloutier',
  UPPER('FR'),
  'Albi',
  81000
);
SET @orgaAddress = LAST_INSERT_ID();

INSERT INTO Contact(address, phoneNumber1) VALUES (
  @orgaContact,
  '0563543734'
);
SET @orgaContact = LAST_INSERT_ID();

INSERT INTO IPerson(lastName, firstName) VALUES (
  UPPER('canivenc'),
  'Laurent'
);
SET @orgaRefPerson = LAST_INSERT_ID();

INSERT INTO External(id, post) VALUES (
  @orgaRefPerson,
  'Infirmier libéral'
);
SET @orgaExternal = LAST_INSERT_ID();

INSERT INTO Organization(name, service, contact, referee) VALUES (
  UPPER('cabinet infirmier liberal'),
  'CSI CANIVENC LAURENT-CHRISTINA YANN-BELT',
  @orgaContact,
  @orgaExternal
);
SET @orga = LAST_INSERT_ID();

-- SELECT * FROM Organization;
-- SELECT post as posts FROM External WHERE post LIKE 'Inf%';
-- SELECT * FROM IPerson;
-- SELECT * FROM Contact;
-- SELECT * FROM Address;

-- --------------------------- --

INSERT INTO InternshipOffer(code, subject, description, startPeriod, endPeriod, organization, tutor) VALUES (
  '20190001',
  'Ceci est un sujet assez intéressant',
  'Ceci est une très longue description improvisée car je n''ai absolument aucune idée d''en quoi peut bien consister le stage de cette entreprise.',
  '2019-05-01',
  '2019-08-01',
  @orga,
  @orgaExternal
);

INSERT INTO InternshipOffer(code, subject, description, startPeriod, endPeriod, organization, tutor) VALUES (
  '20190001',
  'Ceci est un sujet assez intéressant',
  'Ceci est une très longue description improvisée car je n''ai obsolument aucune idée d''en quoi peut bien consister le stage de cette entreprise.',
  '2019-05-01',
  '2019-08-01',
  @orga,
  @orgaExternal
);

SELECT * FROM InternshipOffer;
SELECT MAX(code) + 1 FROM InternshipOffer;

*/