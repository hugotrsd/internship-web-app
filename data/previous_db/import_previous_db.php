<?php

require 'vendor/autoload.php';

$dbname = 'internships';
$host = 'localhost';
$username = 'php';
$password = 'php';

$db = new \App\Database\Database($dbname, $host, $username, $password);

$obj = (object) [
    "name" => "Data",
    "service" => "Data",
    "contact" => (object) [
        "address" => (object) [
            "addressLine1" => "Data",
            "addressLine2" => "Data",
            "addressLine3" => "Data",
            "country" => "FR",
            "state" => "DT",
            "city" => "Albi",
            "postalCode" => "81000"
        ],
        "emailAddress" => "oui@oui.fr",
        "phoneNumber1" => "0612345678",
        "phoneNumber2" => "0612345678",
        "faxNumber" => "0612345678"
    ],
    "referee" => (object) [
        "id" => (object) [
            "lastName" => "Oui",
            "firstName" => "Oui",
            "contact" => null
        ],
        "post" => "Data"
    ]
];

$sup = [
    (object) [
        'id' => (object) [
            'lastName' => 'GARRIC',
            'firstName' => 'Nicolas',
            'contact' => (object) [
                'emailAddress' => 'nicolas.garric@univ-jfc.fr'
            ]
        ]
    ],
    (object) [
        'id' => (object) [
            'lastName' => 'ORTIZ',
            'firstName' => 'Pascal',
            'contact' => (object) [
                'emailAddress' => 'pascal.ortiz@univ-jfc.fr'
            ]
        ]
    ],
    (object) [
        'id' => (object) [
            'lastName' => 'LAFFONT',
            'firstName' => 'Sylvie',
            'contact' => (object) [
                'emailAddress' => 'sylvie.laffont@univ-jfc.fr'
            ]
        ]
    ],
    (object) [
        'id' => (object) [
            'lastName' => 'BRILLON',
            'firstName' => 'Laura',
            'contact' => (object) [
                'emailAddress' => 'laura.brillon@univ-jfc.fr'
            ]
        ]
    ],
];

foreach ($sup as $c) {
    $db->insertSupervisor($c);
}

/*
// Read CSV
$row = 0;
if (($handle = fopen("stages_PC_EEA.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if ($row === 0) continue; // Skip first line
        $row++;



        $num = count($data);
        for ($c = 0; $c < $num; $c++) {
            echo '|' . $data[$c] . '|';
        }
        echo "\n";
    }
    fclose($handle);
}
*/
