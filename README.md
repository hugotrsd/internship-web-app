# Internship web application

### Requirements
This project relies on the dependency manager [Composer](https://getcomposer.org/)

### Installation
* Clone/download the source code.
* Run `composer update` to download and install the dependencies.
* Enable the following php extensions for MySQL: `mysqli` and `pdo_mysql`.

### Running
For development, simply execute `dev_start.sh` from the root of the project.
A PHP development server will be deployed in the current directory, but serving 
files from the `public` directory.

TODO
Setup production environment
