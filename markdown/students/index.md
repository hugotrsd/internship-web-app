# Informations générales sur l'UE de stage TODO

### Présentation {.pt-4}
TODO; Un paragraphe pour expliquer l'UE, sûrement basé sur [ce powerpoint](https://moodle.univ-jfc.fr/pluginfile.php/20192/mod_resource/content/10/Presentation%20UE%20stage.pdf)

### Informations administratives {.pt-4}
##### Informations en ligne {.pt-2}
Les infos administratives pour la procédure de l'établissement de la convention de stage sont accessibles depuis l'ENT, cliquer sur "INTRANET" et dans l'onglet "VOS ÉTUDES" apparaîtra l'onglet "STAGES : MODE D'EMPLOI".

##### Document préalable {.pt-2}
Veuillez regarder le scan ci-dessous pour savoir comment il faut remplir le document préalable. Une fois rempli, l'apporter à l'enseignant référent pour signature. Si vous êtes en anticipation (ordinairement en L2), apporter le formulaire d'autorisation à l'enseignant référent et au responsable de diplôme de la mention pour signature.

### Codes des UE de stages {.pt-4}
* UE de parcours : `16L3PPP500BV`
* UEO S4 : `16L2PPP400BV`
* UEO S6 : `16L3PPP600BV`
* Formation ouverte : `16L1PPPFO012` ou `16L2PPPFO034` ou `16L3PPPFO056`
* TODO Atelier didactique (MAT) : `16L2MAT35FOP` (UE de parcours)

### Documents {.pt-4}
[Comment remplir le document préalable ? (modèle)](https://moodle.univ-jfc.fr/pluginfile.php/42174/mod_resource/content/4/scan_doc_prealable_stage_dec_2019.pdf)