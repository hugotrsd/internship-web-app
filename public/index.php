<?php

declare(strict_types=1);

chdir(dirname(__DIR__));

define('DS', DIRECTORY_SEPARATOR);
define('RUN_DIR', 'temp'); // Path to runtime folder

require 'vendor' . DS . 'autoload.php';

// Only process if this is a server
if (PHP_SAPI === 'cli') {
    print("This application cannot be launched from a CLI. Aborting.\n");
} else {
    $app = new \App\App(getenv('ENV') ?? 'prod', 'config' . DS . 'config.php');
    $app->process(\GuzzleHttp\Psr7\ServerRequest::fromGlobals());
}
