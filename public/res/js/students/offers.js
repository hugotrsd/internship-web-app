$(document).ready(function () {
    // Distance
    function updateDistanceSlider () {
        // Display slider value
        let dist = Number.parseInt($('#distance_slider').val());
        if (dist === 0) 
            $('#distance_value').html('pas de limite');
        else 
            $('#distance_value').html('&le;&nbsp;' + dist + '&nbsp;km');
    };

    function fetchOffers() {
        // Save form data BEFORE disabling every input field
        const formData = $('form').serialize();
        $('form :input').blur(); // Important on mobile -> make the keyboard disappear
        $('form :input').prop("disabled", true);

        $('#offer_status').html($('#offer_status_loading').html());
        $('#offer_placeholder').fadeTo(0, 0.3);

        $.ajax({
            type: 'GET',
            url: '/offers/search',
            data: formData,
            dataType: 'json'
        }).done(function (response) {
            // Split response array into pairs
            if (response['offers'].length === 0) {
                $('#offer_placeholder').html($('#offer_card_empty').html());
            
            } else {
                var splitted = response['offers'].reduce(function (result, value, index, array) {
                    if (index % 2 === 0)
                        result.push(array.slice(index, index + 2));
                    return result;
                }, []);

                var cardsProcessed = Mustache.to_html($('#offer_card_template').html(), splitted);
                $('#offer_placeholder').html(cardsProcessed);

                // Format dates
                timeago().render($('#offer_placeholder .timeago'), 'fr');
                $('.period').each(function () {
                    $(this).html(_prettifiedDate($(this).html(), false));
                });
            }
        }).always(function () {
            $('#offer_status').html('');
            $('#offer_placeholder').fadeTo(200, 1);
            $('form :input').prop("disabled", false);
        }).fail(_alertAJAXError);
    }

    $('#distance_slider').on('input', updateDistanceSlider);

    $('form').submit(function (event) {
        // Disable default behavior because it's a mess
        event.preventDefault();
    });

    $('#form_reset').click(function () {
        $('form').trigger('reset');
        $('form').garlic('destroy');
        fetchOffers();
        updateDistanceSlider(); // Garlic will not trigger an input event
    });

    $('form :input').change(fetchOffers); 

    $('form').garlic();
    updateDistanceSlider();
    fetchOffers();
});