$(document).ready(function () {
    // Format period to something nicer
    const startPeriod = $('#start_period');
    startPeriod.html(_prettifiedDate(startPeriod.html(), false));

    const endPeriod = $('#end_period');
    endPeriod.html(_prettifiedDate(endPeriod.html(), false));

    const createdAt = $('#created_at');
    createdAt.html(_prettifiedDate(createdAt.html(), true));

    const editedAt = $('#edited_at');
    editedAt.html(_prettifiedDate(editedAt.html(), true));
});