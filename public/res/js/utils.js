// Utilities //

function _alertAJAXError(jqXHR, textStatus, errorThrown) {
    alert('AJAX: ' + errorThrown + ': ' + JSON.parse(jqXHR.responseText).error);
};

function _prettifiedDate(dateAsString, withTime = false) {
    const language = 'fr-FR';
    const options = withTime ?
        { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' }
        : { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    return (new Date(dateAsString)).toLocaleDateString(language, options);
}